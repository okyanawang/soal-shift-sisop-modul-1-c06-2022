#!/bin/bash

folder=/home/aaliyah/sisop/soal2/forensic_log_website_daffainfo_log
log=/home/aaliyah/sisop/soal2/log_website_daffainfo.log

#a. membuat folder
mkdir forensic_log_website_daffainfo_log

#b. avg per hour
awk 'BEGIN{FS=":"}
	{NR>1}
	{h[$3]++}
	END {for (x in h){
		N++;
		sum+=h[x];
	}
		avg = sum/N;
		printf "Rata-rata serangan adalah sebanyak %.5f requests per jam\n", avg;
}' $log >> $folder/ratarata.txt

#c. ip terbanyak yg melakukan request dan berapa jumlahnya
awk 'BEGIN{FS=":"}
	{NR>1}
	{if(count[$1]++ >= max)
		max = count[$1];
	}
	END {for(i in count){
		if(max == count[i])
			printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", i, count[i];
	}
}' $log >> $folder/result.txt
	
#d. banyak request yang menggunakan user-agent curl
awk '/curl/{x++}
	END {printf "\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", x;
}' RS=" " $log >> $folder/result.txt

#e. ip yang mengakses website pada 22/jan/2022:02
awk 'FNR == 1 {next}
	BEGIN{FS=":"} 
	{h[$1]++}
	{$2 == "22/Jan/2022"}
	{$3 == "02"}
	END {for (x in h){
		printf "%s\n", x;
	}
}' $log >> $folder/result.txt
	
