<h1>[LAPORAN RESMI MODUL 1 KELOMPOK C06]</h1>

<h2>Daftar Isi</h2>

- [Soal 1](#soal-1) 
	- [a. Membuat script register.sh dan main.sh, serta daftar user dimasukkan di file ./users/user.txt](#a-membuat-script-registersh-dan-mainsh-serta-daftar-user-dimasukkan-di-file-usersusertxt) 
	- [b. Syarat password pada login dan register](#b-syarat-password-pada-login-dan-register-)
	- [c. Format login & register](#c-format-login-register-mmddyy-hhmmss-message-jenis-message-)
	- [d. Command yang dijalankan ketika berhasil login](#d-jika-berhasil-login-ada-2-command-)
- [Soal 2](#soal-2) 
	- [a. Membuat folder forensic_log_website_daffainfo_log](#a-membuat-folder-forensic_log_website_daffainfo_log)
	- [b. Membuat output rata-rata request per jam yang dikirim penyerang](#b-membuat-output-rata-rata-request-per-jam-yang-dikirim-penyerang)
	- [c. Membuat output IP terbanyak yang melakukan request dan jumlah requestnya ke server](#c-membuat-output-ip-terbanyak-yang-melakukan-request-dan-jumlah-requestnya-ke-server)
	- [d. Membuat output jumlah request user-agent curl](#d-membuat-output-jumlah-request-user-agent-curl)
	- [e. Membuat output list IP yang mengakses website pada 22/Jan/2022 pukul 02 pagi](#e-membuat-output-list-ip-yang-mengakses-website-pada-22jan2022-pukul-02-pagi)
- [Soal 3](#soal-3) <br>
	- [Soal 3a](#soal-3a)
	- [Soal 3b](#soal-3b)
	- [Soal 3c](#soal-3c)
	- [Soal 3d](#soal-3d)
- [Kendala](#kendala)
	- [Kendala Soal 1](#kendala-soal-1-)
	- [Kendala Soal 2](#kendala-soal-2-)
	- [Kendala Soal 3](#kendala-soal-3-)

<h2>Soal 1</h2>
<h4><strong>Membuat sistem register dan login</strong></h4>

<h5>a. Membuat script register.sh dan main.sh, serta daftar user dimasukkan di file ./users/user.txt </h5>

<img src="foto/soal1/main.png" alt="main.sh" width="400"> <img src="foto/soal1/register.png" alt="register.sh" width="400">
<br>
Memasukkan username & password ke dalam file user.txt <br>

```
echo $username $password >> ./users/user.txt
```
Dalam user.txt nampak seperti : <br>
<img src="foto/soal1/user.png" alt="user">

<h5>b. Syarat password pada login dan register : </h5>
	<ol type="i">
  		<li>Minimal 8 karakter</li>
  		<li>Memiliki minimal 1 huruf kapital dan 1 huruf kecil</li>
  		<li>Alphanumeric</li>
		<li>Tidak boleh dengan username</li>
	</ol>
Pengecekan menggunakan perulangan :

```
while [[ `expr length "$password"` -lt 8  || "$username" == "$password" || "$password" =~ [^a-zA-Z0-9] || ( ! "$password" =~ [[:upper:]] ) || ( ! "$password" =~ [[:lower:]] ) ]]
```

Agar password tidak terlihat, ketika input :

```
read -s -p "Enter password : " password
```

<h5>c. Format login & register : "MM/DD/YY hh:mm:ss MESSAGE". Jenis message : </h5>
<ol type="i">
<li>Register dengan username yang sudah ada, message : "REGISTER: ERROR User already exists"</li>
Sistem register akan menerima input username dan password dengan syarat username belum pernah ada sebelumnya. Kami menggunakan awk untuk menandai jika username ditemukan dengan nilai 1, kode sebagai berikut :

```
userCheck=$(awk /^$username/' {print "1"}' ./users/user.txt)
```

Setelah itu, memunculkan message :

```
echo -e "${temp} REGISTER: ERROR User already exists!" >> log.txt
```

<li>Register berhasil, message : "REGISTER: INFO User <strong>USERNAME</strong> registered succesfully"</li>

```
echo "${temp} REGISTER: INFO User $username registered succesfully" >> log.txt
```

<li>Login dengan password salah, message : "LOGIN: ERROR Failed login attempt on user <strong>USERNAME</strong>"</li>

Setelah memasukkan username dan password akan diperiksa kesesuaian pasangan username-passwordnya pada file user.txt

```
found=$(awk /^$username.*$password/' {print "1"}' ./users/user.txt)
```

Message yang ditampilkan jika password tidak sesuai : 	

```
echo "${temp} LOGIN: ERROR Failed login attempt on user $username" >> log.txt
```	  

<li>Login berhasil, message : "LOGIN: INFO User <strong>USERNAME</strong> logged in"</li>
</ol>

Jika pengecekan yang ada di atas ditemukan pasangan username-password, maka ditampilkan :

```
echo -e "${temp} LOGIN: INFO User $username logged in" >> log.txt
```

Isi log.txt ialah : <br>
<img src="foto/soal1/log.png" alt="log">

<h5>d. Jika berhasil login, ada 2 command : </h5>
<ol type="i">
  <li>dl N</li>
&emsp;&emsp;= mendownload N gambar dari https://loremflickr.com/320/240 dengan format gambar PIC_XX dan hasilnya dimasukkan ke folder dengan format YYYY-MM-DD_USERNAME. Setelah didownload, akan dizip dengan password yang telah didaftarkan. Jika folder sudah ada, di unzip terlebih dahulu baru ditambahkan gambar.

Sebelum dizip : 
<img src="foto/soal1/before_zip.png" alt="before_zip" width="800">

Setelah dizip :
<img src="foto/soal1/zip.png" alt="zip">

  <li>att</li>
&emsp;&emsp;= menghitung jumlah percobaan login (gagal & berhasil)
</ol>

```
if [[ "$respons" == "att" ]]; then
	awk '
	BEGIN { }
	/LOGIN/ {++n}
	END { print n }' log.txt
else
	temp2=$(date +'%Y-%m-%d')
	if [ -e "${temp2}_$username".zip ]; then
		unzip "${temp2}_$username".zip
	else
	mkdir "${temp2}_$username"
	fi
	cd "${temp2}_$username"/

	for ((i=1; i<=respons1; i=i+1))
	do
		if [[ "$i" -lt 10 ]]; then
			wget --output-document=PIC_0"$i" https://loremflickr.com/320/240
		else
			wget --output-document=PIC_"$i" https://loremflickr.com/320/240
		fi
	done
	cd ..
	zip -P "$password" -r "${temp2}_$username.zip" "${temp2}_$username"
fi
```

<br>

<h2>Soal 2</h2>
Pada soal no. 2 ini digunakannya awk dalam membuat shell script "soal2_forensic_dapos.sh". Command awk perlu menyesuaikan dengan kondisi file log yang diakses (forensic_log_website_daffainfo_log) dan output yang ingin dihasilkan. Berikut preview dari shell script "soal2_forensic_dapos.sh"  <br><br>

<img src="foto/soal2/Code1.png" alt="code1" width="400"> <img src="foto/soal2/Code2.png" alt="code2" width="400">


<h5>a. Membuat folder forensic_log_website_daffainfo_log</h5>

Dalam membuat folder forensic_log_website_daffainfo_log digunakan kode sebagai berikut:<br>

```
mkdir forensic_log_website_daffainfo_log
```

Code tersebut menghasilkan folder baru. Berikut hasilnya

<img src="foto/soal2/a.MembuatFolder.png" alt="a.MembuatFolder" width="800">

Sebelum melanjutkan ke soal berikutnya, dalam mempermudah pengerjaan dibuatlah variable untuk menyingkat beberapa lokasi yang perlu diakses nantinya, yaitu
- Variable <strong>folder</strong> untuk menyingkat lokasi folder <strong>forensic_log_website_daffainfo_log</strong>
```
folder=/home/{direktori masing-masing}/forensic_log_website_daffainfo_log
```
- Variable <strong>log</strong> untuk menyingkat lokasi file <strong>log_website_daffainfo.log</strong>
```
log=/home/{direktori masing-masing}/log_website_daffainfo.log
```

<h5>b. Membuat output rata-rata request per jam yang dikirim penyerang</h5>

Berikut code awk untuk mencari rata-rata request per jam: 

```
awk 'BEGIN{FS=":"}
	{NR>1}
	{h[$3]++}
	END {for (x in h){
		N++;
		sum+=h[x];
	}
		avg = sum/N;
		printf "Rata-rata serangan adalah sebanyak %.5f requests per jam\n", avg;
}'  $log  
```
Keterangan tambahan:
- <strong>BEGIN{FS=":"}</strong> digunakan untuk memisahkan fields/coloumn
- <strong>{NR>1}</strong> digunakan untuk tidak mengexecute baris 1, dikarenakan pada file log baris 1 merupakan headings
- <strong>{h[$3]++}</strong> digunakan untuk membuat array yang mengacu pada column 3. Column 3 memuat data jam 
- <strong>$log</strong> digunakan untuk memanggil variable log yang berisi lokasi file forensic_log_website_daffainfo_log

Selanjutnya, output yang dihasilkan tersebut dimasukkan ke dalam file ratarata.txt dengan menggunakan kode sebagai berikut:

```
>> $folder/ratarata.txt
```

Berikut hasil dari output code di atas

<img src="foto/soal2/b.FileRata-rata.png" alt="b.FileRata-rata" width="800">

<h5>c. Membuat output IP terbanyak yang melakukan request dan jumlah requestnya ke server</h5>

Berikut code awk untuk mencari IP terbanyak yang melakukan request dan berapa jumlahnya:

```
awk 'BEGIN{FS=":"}
	{NR>1}
	{if(count[$1]++ >= max)
		max = count[$1];
	}
	END {for(i in count){
		if(max == count[i])
			printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", i, count[i];
	}
}' $log 
```

Keterangan tambahan:
- <strong>BEGIN{FS=":"}</strong> digunakan untuk memisahkan fields/coloumn
- <strong>{NR>1}</strong> digunakan untuk tidak mengexecute baris 1, dikarenakan pada file log baris 1 merupakan headings
- <strong>if(count[$1]++ >= max)</strong> digunakan untuk mencari jumlah IP terbanyak yang melakukan request. Digunakannya column 1 yang memuat data IP
- <strong>$log</strong> digunakan untuk memanggil variable log yang berisi lokasi file forensic_log_website_daffainfo_log

Selanjutnya, output yang dihasilkan dimasukkan ke dalam file result.txt dengan menggunakan kode sebagai berikut:

```
>> $folder/result.txt
```

Berikut output dari code di atas

<img src="foto/soal2/c.FileResult1.png" alt="c.FileResult1" width="800">

<h5>d. Membuat output jumlah request user-agent curl</h5>

Berikut code awk untuk mencari jumlah request oleh user-agent curl:

```
awk '/curl/{x++}
	END {printf "\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", x;
}' RS=" " $log >> $folder/result.txt
```
Keterangan tambahan:
- <strong>/curl/{x++}</strong> digunakan untuk mencari kata curl pada setiap column
- <strong>$log</strong> digunakan untuk memanggil variable log yang berisi lokasi file forensic_log_website_daffainfo_log

Selanjutnya, output yang dihasilkan dimasukkan ke dalam file result.txt dengan menggunakan kode sebagai berikut:

```
>> $folder/result.txt
```

Berikut output dari code di atas

<img src="foto/soal2/d.FileResult2.png" alt="c.FileResult2" width="800">


<h5>e. Membuat output list IP yang mengakses website pada 22/Jan/2022 pukul 02 pagi</h5>

Berikut code awk untuk melist IP yang mengakses website pada 22/Jan/2022 pukul 02 pagi:

```
awk 'FNR == 1 {next}
	BEGIN{FS=":"} 
	{h[$1]++}
	{$2 == "22/Jan/2022"}
	{$3 == "02"}
	END {for (x in h){
		printf "%s\n", x;
	}
}' $log >> $folder/result.txt
```

Keterangan tambahan:
- <strong>FNR == 1 {next}</strong> digunakan untuk tidak mengexecute baris 1, dikarenakan pada file log baris 1 merupakan headings
- <strong>BEGIN{FS=":"}</strong> digunakan untuk memisahkan fields/coloumn
- <strong>{h[$1]++}</strong> digunakan untuk membuat array yang mengacu pada column 3. Column 3 memuat data IP
- <strong>{$2 == "22/Jan/2022"}</strong> digunakan untuk memilih column 2 yang memuat data tanggal. Dari column tersebut dicari tanggal "22/Jan/2022"
- <strong>{$3 == "02"}</strong> digunakan untuk memilih column 3 yang memuat data jam. Dari column tersebut dicari jam "02" sesuai yang diminta oleh soal
- <strong>$log</strong> digunakan untuk memanggil variable log yang berisi lokasi file forensic_log_website_daffainfo_log

Selanjutnya, output yang dihasilkan dimasukkan ke dalam file result.txt dengan menggunakan kode sebagai berikut:

```
>> $folder/result.txt
```

Berikut output dari code di atas

<img src="foto/soal2/e.FileResult3.png" alt="c.FileResult3" width="800">

<br>

<h2>Soal 3</h2>
<strong>Membuat monitoring resources program khusus ram dan size suatu direktori </strong>

<h4>Soal 3a</h4>
Simpan data metric ram dan size direktori pada suatu variabel. Ambil data tanggal dan waktu saat ini menggunakan fungsi date dan simpan juga pada variabel

```
ambil_tanggal="metrics_"$(date +"%Y%m%d%H%M%S")
ram=$(free -m)
pathSize=$(du -sh $HOME)
babah=$ram" "$pathSize
```

Sesuaikan isi log dengan permintaan soal. Gunakan pipe untuk menjadikan output dari suatu perintah sebagai input perintah selanjutnya, lalu gunakan sed untuk me-replace suatu substring dengan yang diinginkan, gunakan fungsi tr untuk mengubah seluruh abjad kapital dengan abjad kecil. Gunakan juga perulangan dalam awk, yang mana dalam setiap perulangannya kita ambil kolom string yang sesuai dan menatanya sesuai permintaan soal. Terakhir kita masukkan hasil tadi ke file log.
```
echo $babah | sed 's/\/cache//g' | tr '[:upper:]' '[:lower:]' | sed 's/://g' | awk '{ 
    for (i = 1; i <= 6; i++)
        printf ($7"_"$i",");
    for (j = 1; j <= 3; j++)
        printf ($14"_"$j",");
    printf ("path,path_size\n");
    for (k = 8; k <= 13; k++)
        printf ($k",")
    for (mafu = 15; mafu <= 17; mafu++)
        printf ($mafu",")
    printf ($19"/test/,"$18"\n")
}' >> $HOME/log/$ambil_tanggal.log
```

Bukti isi file minute_log:
<img src="foto/soal3/3a.png" alt="3a">

Bukti hasil file minute_log:
<img src="foto/soal3/3a2.png" alt="3a2">


<h4>Soal 3b</h4>
Agar berjalan otomatis per menit, set crontab pada perintah sebagai berikut

```
* * * * * bash $HOME/{Direktori masing-masing}/soal3/minute_log.sh
```

Bukti :
<img src="foto/soal3/3b.png" alt="3b">


<h4>Soal 3c</h4>
Membuat file aggregasi untuk menghitung rata-rata, maximum, minimum dari hasil log metrics selama satu jam

Buat variabel filter yang diambil dari data jam sekarang, fungsi filter ini adalah untuk menyaring file yang masuk dalam hitungan 1 jam yang akan dihitung
```
ambil_jam=$(date +"%H")
ambil_tanggal=$(date +"%Y%m%d")

let filter_fix=$ambil_tanggal$ambil_jam-1
```

Ambil file yang sesuai dengan filter yang telah dibuat, pisahkan header(yang berisi nama metricnya masing-masing) dengan valuenya(nilai dari matriks itu). Lalu ubah format penulisan dari yang awalnya tiap metrics dipisahkan koma menjadi dipisahkan spasi agar memudahkan dalam pengambilan data selanjutnya
```
newMetrics=$(ls $HOME/log/metrics_$filter_fix* | awk '{system("cat " $1)}')
valueMetrics=$(echo "$newMetrics" | sed -n 'n;p' | sed 's/,/ /g' | awk '{print substr($0,1,length($0)-3)","substr($0,length($0)-1,1)}')
headerMetrics=$(echo "$newMetrics" | sed -n 'p;n' | sed 's/,/ /g' | awk -F '\t' 'NR==1 && NF=1' )
headerMetrics="type $headerMetrics"
```

Hitung masing-masing fungsi aggreratenya manual satu per satu dari setiap jenis metrics, dengan bantuan if dalam awk untuk perhitungan data
```
echo "$valueMetrics" | awk '{print $1}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_total.txt
echo "$valueMetrics" | awk '{print $2}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_used.txt
echo "$valueMetrics" | awk '{print $3}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_free.txt
echo "$valueMetrics" | awk '{print $4}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_shared.txt
echo "$valueMetrics" | awk '{print $5}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_buff.txt
echo "$valueMetrics" | awk '{print $6}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_available.txt
echo "$valueMetrics" | awk '{print $7}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_total.txt
echo "$valueMetrics" | awk '{print $8}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_used.txt
echo "$valueMetrics" | awk '{print $9}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_free.txt
echo "$valueMetrics" | awk '{print $10}' | awk 'BEGIN {max = 1} {if (max<4) print $1"-";max++}' > path.txt
echo "$valueMetrics" | awk '{print $11}' | sed 's/[,gG]//g' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's_\([0-9]\)\([0-9]\)_\1,\2G_' > path_size.txt
printf "minimum-\nmaximum-\naverage-" > type.txt
```

Simpan hasil ke dalam satu variabel, lalu sesuaikan formatnya dengan permintaan soal
```
bodyMetrics=$(paste type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt)

bodyMetrics=$(echo $bodyMetrics | sed 's/- /,/g' | sed 's/,maximum/\nmaximum/g' | sed 's/,average/\naverage/g' | sed 's/G-/G/g')
```

Hapus file yang tidak diperlukan, lalu masukkan hasil tadi pada file log :
```
rm -f type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt

echo -e "$headerMetrics\n$bodyMetrics" | sed 's/[\ ]/,/g' > $HOME/log/metrics_agg_$filter_fix.log
```

Pasang crontab dengan script berikut untuk membuat agregasi file log ke satuan jam
```
0 * * * * bash $HOME/{Sesuaikan direktori masing-masing}/soal3/aggregate_minutes_to_hourly_log.sh
```

Bukti hasil file aggregasi : 
<img src="foto/soal3/3c2.png" alt="3c2">

Bukti isi file aggregasi :
<img src="foto/soal3/3c.png" alt="3c">


<h4>Soal 3d</h4>
Gunakan chmod untuk mengubah akses suatu file terhadap suatu user, dalam hal ini, hanya user yang dapat membaca soal, jadi akses membaca hanya diberikan kepada user dengan kode 7. Untuk userlain hanya bisa me-write dan execute tanpa membacanya

Pada file aggregate_minutes_to_hourly_log.sh
```
chmod 733 $HOME/log/metrics_agg_$filter_fix.log
```

Pada file minute_log.sh
```
chmod 733 $HOME/log/$ambil_tanggal.log
```

Bukti setting chmod pada file minute_log :
<img src="foto/soal3/3d1.png" alt="3d1">

Bukti setting chmod pada file aggregate :
<img src="foto/soal3/3d2.png" alt="3d2">

<h2>Kendala</h2>
Dalam pengerjaan soal-soal di atas ditemui beberapa kendala yang akan dijabarkan berdasarkan soal. Berikut kendala-kendala yang ditemui:<br>
<h5>Kendala Soal 1 : </h5>
<ol type="i">
  <li>Tidak terbiasa menggunakan ubuntu dan terminalnya sehingga perlu banyak adaptasi</li>
  <li>Banyak syntax yang baru diketahui dan dipelajari sehingga sedikit kesusahan untuk mengerjakan</li>
  <li>Terkendala dalam pemahaman soal karena cukup kaget dengan soal shift</li>
  <li>Sempat berkali-kali gagal dalam perulangan untuk pengecekan password sesuai kriteria</li>
  <li>Kesusahan di bagian mendownload gambar dan diletakkan di dalam folder lalu dizip dengan password. Di awal-awal hanya bisa mendownload tanpa dizip</li>
  <li>Sedikit bingung juga dengan penggunaan gitlab</li>
</ol>

<h5>Kendala Soal 2 : </h5>
<ol type="i">
	<li>Kesulitan dalam mengimpletasikan soal kedalam code karena tidak pernah menggunakan bahasa shell</li>
	<li>Perlu mencari satu-persatu syntax yang dibutuhkan dan dicoba hingga menemukan yang pas. Hal ini memakan banyak waktu</li>
	<li>Kebingungan pada soal no. 1 karena perhitungan yang didapat saat menghitung manual dan menggunakan code berbeda. Hal ini disebabkan oleh baris 1 (headings) ikut terhitung</li>
	<li>Kebingungan dalam menggunakan gitlab</li>
</ol>

<h5>Kendala Soal 3 : </h5>
<ol type="i">
	<li>Kesulitan memasukkan keyword yang tepat untuk mencari fungsi yang diinginkan seperti penggunaan percabangan dan perulangan dalam awk</li>
	<li>Kesulitan mengoperasikan karena shell scripting sangat space sensitive</li>
</ol>
