#!/bin/bash

ambil_jam=$(date +"%H")
ambil_tanggal=$(date +"%Y%m%d")

let filter_fix=$ambil_tanggal$ambil_jam-1

newMetrics=$(ls $HOME/log/metrics_$filter_fix* | awk '{system("cat " $1)}')
valueMetrics=$(echo "$newMetrics" | sed -n 'n;p' | sed 's/,/ /g' | awk '{print substr($0,1,length($0)-3)","substr($0,length($0)-1,1)}')
headerMetrics=$(echo "$newMetrics" | sed -n 'p;n' | sed 's/,/ /g' | awk -F '\t' 'NR==1 && NF=1' )
headerMetrics="type $headerMetrics"

echo "$valueMetrics" | awk '{print $1}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_total.txt
echo "$valueMetrics" | awk '{print $2}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_used.txt
echo "$valueMetrics" | awk '{print $3}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_free.txt
echo "$valueMetrics" | awk '{print $4}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_shared.txt
echo "$valueMetrics" | awk '{print $5}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_buff.txt
echo "$valueMetrics" | awk '{print $6}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_available.txt
echo "$valueMetrics" | awk '{print $7}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_total.txt
echo "$valueMetrics" | awk '{print $8}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_used.txt
echo "$valueMetrics" | awk '{print $9}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_free.txt
echo "$valueMetrics" | awk '{print $10}' | awk 'BEGIN {max = 1} {if (max<4) print $1"-";max++}' > path.txt
echo "$valueMetrics" | awk '{print $11}' | sed 's/[,gG]//g' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's_\([0-9]\)\([0-9]\)_\1,\2G_' > path_size.txt
printf "minimum-\nmaximum-\naverage-" > type.txt

bodyMetrics=$(paste type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt)

bodyMetrics=$(echo $bodyMetrics | sed 's/- /,/g' | sed 's/,maximum/\nmaximum/g' | sed 's/,average/\naverage/g' | sed 's/G-/G/g')

rm -f type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt

echo -e "$headerMetrics\n$bodyMetrics" | sed 's/[\ ]/,/g' > $HOME/log/metrics_agg_$filter_fix.log

chmod 733 $HOME/log/metrics_agg_$filter_fix.log

#Pasang crontab dengan script dibawah ini untuk membuat agregasi file log ke satuan jam
#0 * * * * bash $HOME/{Sesuaikan direktori masing-masing}/soal3/aggregate_minutes_to_hourly_log.sh