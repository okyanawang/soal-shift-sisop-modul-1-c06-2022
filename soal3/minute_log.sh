#!/bin/bash

ambil_tanggal="metrics_"$(date +"%Y%m%d%H%M%S")
ram=$(free -m)
pathSize=$(du -sh $HOME)
babah=$ram" "$pathSize

echo $babah | sed 's/\/cache//g' | tr '[:upper:]' '[:lower:]' | sed 's/://g' | awk '{ 
    for (i = 1; i <= 6; i++)
        printf ($7"_"$i",");
    for (j = 1; j <= 3; j++)
        printf ($14"_"$j",");
    printf ("path,path_size\n");
    for (k = 8; k <= 13; k++)
        printf ($k",")
    for (mafu = 15; mafu <= 17; mafu++)
        printf ($mafu",")
    printf ($19"/test/,"$18"\n")
}' >> $HOME/log/$ambil_tanggal.log

chmod 733 $HOME/log/$ambil_tanggal.log

# -> Soal 3b gunakan script di bawah ini untuk menjalankan crontab
# -> * * * * * bash $HOME/{Direktori masing-masing}/soal3/minute_log.sh