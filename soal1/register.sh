#!/bin/bash

read -p "Enter username : " username
temp=$(date +'%m/%d/%Y %H:%M:%S')

userCheck=$(awk /^$username/' {print "1"}' ./users/user.txt)

while [[ userCheck -eq "1" ]]
do
	echo "Username sudah pernah ada, masukkan username lain"
	echo -e "${temp} REGISTER: ERROR User already exists!" >> log.txt
	read -p "Enter username : " username
	userCheck=$(awk /^$username/' {print "1"}' ./users/user.txt)
done

read -s -p "Enter password : " password

while [[ `expr length "$password"` -lt 8  || "$username" == "$password" || "$password" =~ [^a-zA-Z0-9] || ( ! "$password" =~ [[:upper:]] ) || ( ! "$password" =~ [[:lower:]] ) ]]
do
	echo -e ""
	if [ `expr length "$password"` -lt 8 ]; then
		echo "[WARNING] Password harus lebih dari 8 karakter"
	fi
	if [ "$username" == "$password" ]; then
		echo "[WARNING] Password dan username harus berbeda"
	fi
	if [[ "$password" =~ ^[a-zA-Z0-9] ||  ( ! "$password" =~ [[:upper:]] ) || ( ! "$password" =~ [[:lower:]] ) ]]; then
		echo "[WARNING] Password harus mengandung huruf kapital dan huruf kecil, dan/atau angka"
	fi

	read -s -p "Enter password : " password
done

echo $username $password >> ./users/user.txt
echo -e "\nBerhasil melakukan pendaftaran"
echo "${temp} REGISTER: INFO User $username registered succesfully" >> log.txt
