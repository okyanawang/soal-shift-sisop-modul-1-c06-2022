#!/bin/bash

echo "SILAHKAN LOG IN!"

read -p "Enter username : " username
read -s -p "Enter password : " password
temp=$(date +'%m/%d/%Y %H:%M:%S')
found=$(awk /^$username.*$password/' {print "1"}' ./users/user.txt)

if [[ "$found" -eq 1 ]]; then
	echo -e "\nAnda berhasil login!"
	echo -e "${temp} LOGIN: INFO User $username logged in" >> log.txt
	read -p "Masukkan perintah (dl N / att) : " respons respons1

	if [[ "$respons" == "att" ]]; then
		awk '
		BEGIN { }
		/LOGIN/ {++n}
		END { print n }' log.txt

	else
		temp2=$(date +'%Y-%m-%d')
		if [ -e "${temp2}_$username".zip ]; then
			unzip "${temp2}_$username".zip
		else
		mkdir "${temp2}_$username"
		fi
		cd "${temp2}_$username"/

		for ((i=1; i<=respons1; i=i+1))
		do
			if [[ "$i" -lt 10 ]]; then
				wget --output-document=PIC_0"$i" https://loremflickr.com/320/240
			else
				wget --output-document=PIC_"$i" https://loremflickr.com/320/240
			fi
		done
		cd ..
		zip -P "$password" -r "${temp2}_$username.zip" "${temp2}_$username"
		fi

else
	echo -e "\nUsername/Password salah"
	echo "${temp} LOGIN: ERROR Failed login attempt on user $username" >> log.txt
fi
